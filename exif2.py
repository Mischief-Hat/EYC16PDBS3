#! /usr/bin/python2
import pyexiv2
import sys, os
import re

image_file = sys.argv[1]
print(image_file)
flag=0
patterns = ['Adobe','adobe','ADOBE','photoshop','Photoshop','Affinity','AFFINITY','affinity','PaintShop','PAINTSHOP','paintshop','Acorn','PhotLab','Camera+','Pixelmator ','Pixlr','GIMP','Paint.net','Sumo Paint','Aviary']
data = pyexiv2.metadata.ImageMetadata(image_file)
data.read()
for key in data.exif_keys:
 tag = data[key]
 s=key
 k=tag.value
 if 'Software' in s:
  for p in patterns:
    if re.search(p,k):
        flag=1
file=open("exif_result.txt","w")
file.write(str(flag))
file.close()