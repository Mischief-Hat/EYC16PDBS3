Tested on Ubuntu 16.04

Tkinter is required for the GUI to work
		sudo apt-get install python3-tk
1. Install all the dependencies from requirements.txt file via pip
		pip install -r requirements.txt 
2. Run deep_blue.py
3. Select pre-surgery and post-surgery images respectively from the dataset folder for comparison and view the results
4. Respectively view the forgery detection

Face recognition -
Images are aligned using 68 facial keypoints 
128D encodings are generated for every face using resnet-34
L2 Metric is used for comparison

Forgery detection - 
Jpeg quantization is used 


Download the models from here - https://drive.google.com/open?id=1m5HQdggNCVt7_1nUG9tuYSfzyVqQDM3I
 
Place it in the main repository folder


Screenshots are attached in the repository
