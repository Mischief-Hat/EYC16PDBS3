import numpy
import dlib
import glob
import math
import sys
import cv2
import os
import quantize
from tkinter.filedialog import askopenfilename

from tkinter import *
from tkinter import messagebox, ttk
from PIL import Image, ImageTk

FACE_POINTS = list(range(17, 68))
MOUTH_POINTS = list(range(48, 61))
RIGHT_BROW_POINTS = list(range(17, 22))
LEFT_BROW_POINTS = list(range(22, 27))
RIGHT_EYE_POINTS = list(range(36, 42))
LEFT_EYE_POINTS = list(range(42, 48))
NOSE_POINTS = list(range(27, 35))
JAW_POINTS = list(range(0, 17))

# Points used to line up the images.
ALIGN_POINTS = (LEFT_BROW_POINTS + RIGHT_EYE_POINTS + LEFT_EYE_POINTS +
							   RIGHT_BROW_POINTS + NOSE_POINTS + MOUTH_POINTS)

pre_detector = dlib.get_frontal_face_detector()
pre_predictor = dlib.shape_predictor('dlib/shape_predictor_68_face_landmarks.dat')

post_detector = dlib.simple_object_detector('dlib/face_detector1.svm')
post_predictor = dlib.shape_predictor('dlib/predictor.dat')

facerec = dlib.face_recognition_model_v1('dlib/recog.dat')


##########################################################################

class TooManyFaces(Exception):
	pass

class NoFaces(Exception):
	pass

def get_post_landmarks(im, rect=None):
	if rect is None:
		rects = post_detector(im, 1)
	
		if len(rects) > 1:
			raise TooManyFaces
		if len(rects) == 0:
			raise NoFaces

		rect = rects[0]

	return numpy.matrix([[p.x, p.y] for p in post_predictor(im, rect).parts()])

def get_pre_landmarks(im, rect=None):
	if rect is None:
		rects = pre_detector(im, 1)
	
		if len(rects) > 1:
			raise TooManyFaces
		if len(rects) == 0:
			raise NoFaces

		rect = rects[0]

	return numpy.matrix([[p.x, p.y] for p in pre_predictor(im, rect).parts()])

def get_landmarks(pre, post):
	landmarks1 = get_pre_landmarks(pre)
	landmarks2 = get_post_landmarks(post)

	return landmarks1, landmarks2

def face_distance(face_encodings, face_to_compare):
    """
    Given a list of face encodings, compare them to a known face encoding and get a euclidean distance
    for each comparison face. The distance tells you how similar the faces are.
    :param faces: List of face encodings to compare
    :param face_to_compare: A face encoding to compare against
    :return: A numpy ndarray with the distance for each face in the same order as the 'faces' array
    """
    if len(face_encodings) == 0:
        return numpy.empty((0))

    return numpy.linalg.norm(face_encodings - face_to_compare, axis=1)

def compare_faces(known_face_encodings, face_encoding_to_check, tolerance=0.4):
	"""
	Compare a list of face encodings against a candidate encoding to see if they match.
	:param known_face_encodings: A list of known face encodings
	:param face_encoding_to_check: A single face encoding to compare against the list
	:param tolerance: How much distance between faces to consider it a match. Lower is more strict. 0.6 is typical best performance.
	:return: A list of True/False values indicating which known_face_encodings match the face encoding to check
	"""
	return list(face_distance(known_face_encodings, face_encoding_to_check) <= tolerance)

####################################################################################

def transformation_from_points(points1, points2):
	"""
	Return an affine transformation [s * R | T] such that:

		sum ||s*R*p1,i + T - p2,i||^2

	is minimized.

	"""
	# Solve the procrustes problem by subtracting centroids, scaling by the
	# standard deviation, and then using the SVD to calculate the rotation. See
	# the following for more details:
	#   https://en.wikipedia.org/wiki/Orthogonal_Procrustes_problem

	points1 = points1.astype(numpy.float64)
	points2 = points2.astype(numpy.float64)

	c1 = numpy.mean(points1, axis=0)
	c2 = numpy.mean(points2, axis=0)
	points1 -= c1
	points2 -= c2

	s1 = numpy.std(points1)
	s2 = numpy.std(points2)
	points1 /= s1
	points2 /= s2

	U, S, Vt = numpy.linalg.svd(points1.T * points2)

	# The R we seek is in fact the transpose of the one given by U * Vt. This
	# is because the above formulation assumes the matrix goes on the right
	# (with row vectors) where as our solution requires the matrix to be on the
	# left (with column vectors).
	R = (U * Vt).T

	return numpy.vstack([numpy.hstack(((s2 / s1) * R,
									   c2.T - (s2 / s1) * R * c1.T)),
						 numpy.matrix([0., 0., 1.])])


def warp_im(im, M, dshape):
	output_im = numpy.zeros(dshape, dtype=im.dtype)

	cv2.warpAffine(im,
				   M[:2],
				   (dshape[1], dshape[0]),
				   dst=output_im,
				   borderMode=cv2.BORDER_TRANSPARENT,
				   flags=cv2.WARP_INVERSE_MAP)
	return output_im

###########################################################################
#Eye patch filling code

def draw_convex_hull(im, points, color):
	points = cv2.convexHull(points)
	cv2.fillConvexPoly(im, points, color=color)

def get_face_mask(im, landmarks):
	im = numpy.zeros(im.shape[:2], dtype=numpy.float64)
	righteye = numpy.empty((12,2))
	righteye = righteye.astype(numpy.int)
	for i in range(2):
		righteye[0][i] = landmarks[22][i]
		righteye[1][i] = landmarks[23][i]
		righteye[2][i] = landmarks[24][i]
		righteye[3][i] = landmarks[25][i]
		righteye[4][i] = landmarks[26][i]
		righteye[5][i] = landmarks[16][i]
		righteye[6][i] = landmarks[14][i]
		righteye[7][i] = landmarks[35][i]
		righteye[8][i] = landmarks[30][i]
		righteye[9][i] = landmarks[29][i]
		righteye[10][i] = landmarks[28][i]
		righteye[11][i] = landmarks[27][i]
	for i in range(5):
		righteye[i][1]-= 10
	righteye[0][0]-= 7
	righteye[11][0]-= 5
	righteye[10][0]-= 3
	righteye[9][0]-= 3
	righteye[8][0]-= 3
	# for i in range(2):
	#   righteye[0][i] = landmarks[42][i]
	#   righteye[1][i] = landmarks[43][i]
	#   righteye[2][i] = landmarks[44][i]
	#   righteye[3][i] = landmarks[45][i]
	#   righteye[4][i] = landmarks[46][i]
	#   righteye[5][i] = landmarks[47][i]
	# print(righteye)
	draw_convex_hull(im,righteye,color=1)

	im = numpy.array([im, im, im]).transpose((1, 2, 0))

	im = (cv2.GaussianBlur(im, (15, 15), 0) > 0) * 1.0
	im = cv2.GaussianBlur(im, (15, 15), 0)
	#15 is Feather Amount
	#cv2.imshow('im',im)
	#cv2.waitKey(0)
	im = im[:,:,0]*0.2989 + im[:,:,1]*0.5870 + im[:,:,2]*0.1140
	return im





def flipIndices(fliplandmarks):
	#Flipping of indices
	#Jaw flip
	z = 16
	for i in range(8):
		k = i + z
		for j in range(2):
			temp = fliplandmarks[i][j]
			fliplandmarks[i][j] = fliplandmarks[k][j]
			fliplandmarks[k][j] = temp
		k = 0
		z = z - 2
	#Eyebrow flip
	z = 9
	for i in range(17,22):
		k = i + z
		for j in range(2):
			temp = fliplandmarks[i][j]
			fliplandmarks[i][j] = fliplandmarks[k][j]
			fliplandmarks[k][j] = temp
		k = 0
		z = z - 2
	#Eyeflip - upper
	z = 9
	for i in range(36,40):
		k = i + z
		for j in range(2):
			temp = fliplandmarks[i][j]
			fliplandmarks[i][j] = fliplandmarks[k][j]
			fliplandmarks[k][j] = temp
		k = 0
		z = z - 2
	#Eyeflip - lower
	z = 7
	for i in range(40,42):
		k = i + z
		for j in range(2):
			temp = fliplandmarks[i][j]
			fliplandmarks[i][j] = fliplandmarks[k][j]
			fliplandmarks[k][j] = temp
		k = 0
		z = z - 2
	#Noseflip
	z = 4
	for i in range(31,33):
		k = i + z
		for j in range(2):
			temp = fliplandmarks[i][j]
			fliplandmarks[i][j] = fliplandmarks[k][j]
			fliplandmarks[k][j] = temp
		k = 0
		z = z - 2   
	#Mouthflip - upper
	z = 6   
	for i in range(48,51):
		k = i + z
		for j in range(2):
			temp = fliplandmarks[i][j]
			fliplandmarks[i][j] = fliplandmarks[k][j]
			fliplandmarks[k][j] = temp
		k = 0
		z = z - 2
	#Mouthflip - bottom
	z = 2
	for i in range(58,60):
		k = i - z
		for j in range(2):
			temp = fliplandmarks[i][j]
			fliplandmarks[i][j] = fliplandmarks[k][j]
			fliplandmarks[k][j] = temp
		k = 0
		z = z + 2   
	#Lip - top
	z = 4   
	for i in range(60,62):
		k = i + z
		for j in range(2):
			temp = fliplandmarks[i][j]
			fliplandmarks[i][j] = fliplandmarks[k][j]
			fliplandmarks[k][j] = temp
		k = 0
		z = z - 2   
	#Lip - bottom
	z = 2
	for i in range(67,68):
		k = i - z
		for j in range(2):
			temp = fliplandmarks[i][j]
			fliplandmarks[i][j] = fliplandmarks[k][j]
			fliplandmarks[k][j] = temp
		k = 0
		z = z + 2
	return fliplandmarks


def convert_nparray_to_matrix(points1,points2):
	matrix1 = numpy.matrix([[points1[r][0],points1[r][1]] for r in range(points1.shape[0])])
	matrix2 = numpy.matrix([[points2[r][0],points2[r][1]] for r in range(points2.shape[0])])
	return(matrix1,matrix2)


def landmarks_find(img,flipfull,rect):

	shape = post_predictor(img,rect)
	landmarks = numpy.empty((68,2),dtype=numpy.int)
	fliplandmarks = numpy.empty((68,2),dtype=numpy.int)
	for i in range(68):
		(landmarks[i][0],landmarks[i][1]) = (shape.part(i).x, shape.part(i).y)

		fliplandmarks[i][0] =(img.shape[1]) - landmarks[i][0]  
		fliplandmarks[i][1] = landmarks[i][1]

	fliplandmarks = flipIndices(fliplandmarks)
	return img,flipfull,landmarks,fliplandmarks



def fill_eye_patch(img, flipfull, rect):

	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	flipfull = cv2.cvtColor(flipfull, cv2.COLOR_BGR2GRAY)

	img,flipfull,landmarks,fliplandmarks = landmarks_find(img,flipfull,rect)

	(points1,points2) = convert_nparray_to_matrix(landmarks,fliplandmarks)
	M = transformation_from_points(points1,points2)
	mask = get_face_mask(flipfull,fliplandmarks)
	warped_mask = warp_im(mask,M,img.shape)
	warped_img2 = warp_im(flipfull,M,img.shape)
	output_im = img * (1 - warped_mask) + warped_img2 * warped_mask
	output_im = output_im.astype(numpy.uint8)
	cv2.imwrite("output.jpg", output_im)

	output_im = cv2.cvtColor(output_im, cv2.COLOR_GRAY2RGB)

	return output_im


#############################################################################



def resize(pre, post, template, rect):

	landmarks = get_pre_landmarks(template, rect)

	landmarks1, landmarks2 = get_landmarks(pre, post)

	M = transformation_from_points(landmarks[ALIGN_POINTS], landmarks1[ALIGN_POINTS])
	pre = warp_im(pre, M, template.shape)

	M = transformation_from_points(landmarks[ALIGN_POINTS], landmarks2[ALIGN_POINTS])
	post = warp_im(post, M, template.shape)

	return pre, post


def annotate_landmarks(pre, post, rect=None):

	pre = pre.copy()
	landmarks = get_pre_landmarks(pre, rect)
	for idx, point in enumerate(landmarks):
		pos = (point[0, 0], point[0, 1])
		cv2.circle(pre, pos, 1, (255, 255, 255), -1)

	post = post.copy()
	landmarks = get_post_landmarks(post, rect)
	for idx, point in enumerate(landmarks):
		pos = (point[0, 0], point[0, 1])
		cv2.circle(post, pos, 1, (255, 255, 255), -1)
	
	return pre, post

def get_pre_encodings(img, rect=None, jitter=10):

	if rect is None:
		rect = post_detector(img)[0]

	landmarks = post_predictor(img, rect)

	return numpy.array(facerec.compute_face_descriptor(img, landmarks, jitter))


def get_post_encodings(img, rect=None, jitter=10):

	if rect is None:
		rect = post_detector(img)[0]

	landmarks = post_predictor(img, rect)

	return numpy.array(facerec.compute_face_descriptor(img, landmarks, jitter))    

def save_encodings(pre_name, pre_encodings, post_name, post_encodings):

	global pre_path
	global post_path

	f = open("encodings/pre_encodings.txt", "a")
	f.write(str(pre_name) + " ")
	for j in range(0,128):
		f.write(str(pre_encodings[j]) + " ")
	f.write('\n')
	f.close()

	f = open("encodings/post_encodings.txt", "a")
	f.write(str(post_name) + " ")
	for j in range(0,128):
		f.write(str(post_encodings[j]) + " ")
	f.write('\n')
	f.close()

	im1 = cv2.imread(pre_path, 1)
	im2 = cv2.imread(post_path, 1)

	cv2.imwrite("new_dataset/pre/"+pre_name, im1)
	cv2.imwrite("new_dataset/post/"+post_name, im2)


def check_if_already_present(encodings, type):

	isPresent = False
	file_name = ""

	if type=="pre":
		file = "encodings/pre_encodings.txt"
	else:
		file = "encodings/post_encodings.txt"

	biden_encoding = numpy.zeros(128, dtype=numpy.float64)
	with open(file,"r") as f2:
		inner_loop=1
		for i,line in enumerate(f2):
			line = line.split(' ')
			file_name = line[0]
			for j in range(0,128):
				biden_encoding[j] = numpy.float64(line[j+1])
						
			result = compare_faces([biden_encoding], encodings)
			distance = face_distance([biden_encoding], encodings)
			if distance < 0.2:
				isPresent = True
				print(file_name, distance)

	return isPresent, file_name


def showImage(pre, post):
	cv2.imshow("pre", pre)
	cv2.imshow("post", post)
	cv2.waitKey(0)


def histoEqu(img):
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	clahe = cv2.createCLAHE(clipLimit=1.0, tileGridSize=(2,2))
	img = clahe.apply(img)
	img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)

	return img

def adjust_gamma(image, gamma=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
	invGamma = 1.0 / gamma
	table = numpy.array([((i / 255.0) ** invGamma) * 255
		for i in numpy.arange(0, 256)]).astype("uint8")
	
	return cv2.LUT(image, table)

################################################################################

def pre_forgery_detection():
	global pre_path
	try:
		result = quantize.load(pre_path)
	except AttributeError:
		print("")
	try:
		if result==True:
			msg = messagebox.showinfo("Forgery", "Images is manipulated")
		else:
			msg = messagebox.showinfo("Forgery", "Image is original")
	except UnboundLocalError:
		print("")

def post_forgery_detection():
	global post_path
	try:
		result = quantize.load(post_path)
	except AttributeError:
		print("")
	try:
		if result==True:
			msg = messagebox.showinfo("Forgery", "Images is manipulated")
		else:
			msg = messagebox.showinfo("Forgery", "Image is original")
	except UnboundLocalError:
		print("")

def compare():

	global pre
	global post
	global pre_encodings
	global post_encodings
	global template
	global rect
	global pre_canvas
	global post_canvas

	try:
		pre, post= resize(pre, post, template, rect)

		pre = histoEqu(pre)
		post = histoEqu(post)

		post_flip = cv2.flip(post,1)
		post = fill_eye_patch(post, post_flip, rect)
	   
		pre_encodings = get_pre_encodings(pre, rect)
		post_encodings = get_post_encodings(post, rect)


		distance = numpy.linalg.norm(pre_encodings - post_encodings, axis=0)

		pre, post = annotate_landmarks(pre, post, rect)

		cv2.imwrite("pre.jpg",pre)
		cv2.imwrite("post.jpg",post)

		pre = Image.open("pre.jpg")
		pre = pre.resize((200,200), Image.ANTIALIAS)
		pre = ImageTk.PhotoImage(pre)
		item = pre_canvas.create_image(0, 0, anchor=NW, image=pre)

		post = Image.open("post.jpg")
		post = post.resize((200,200), Image.ANTIALIAS)
		post = ImageTk.PhotoImage(post)
		item = post_canvas.create_image(0, 0, anchor=NW, image=post)

		if distance < 0.46:
			msg = messagebox.showinfo("Compare", "Same Person" + str(distance))
		else:
			msg = messagebox.showinfo("Compare", "Different Persons" + str(distance))

	except NoFaces:
		msg = messagebox.showinfo("Compare", "No Face Found")
	except TooManyFaces:
		msg = messagebox.showinfo("Compare", "TooManyFaces")
	except Exception as e:
		print(e)

def compare_with_pre_dataset():

	global pre
	global pre_encodings

	isPresent, file_name = check_if_already_present(pre_encodings, "pre")

	if isPresent == True:
		msg = messagebox.showinfo("Pre Dataset", "Found ")
	else:
		msg = messagebox.showinfo("Compare", "Not Found")

def compare_with_post_dataset():

	global post
	global post_encodings

	isPresent, file_name = check_if_already_present(post_encodings, "post")

	if isPresent == True:
		#create_window(file_name)
		msg = messagebox.showinfo("Post Dataset", "Found ")
	else:
		msg = messagebox.showinfo("Compare", "Not Found")

'''def create_window(file_name):
	global top
	global post_path
	global path

	head, tail = os.path.split(post_path)
	path = head + '/' + file_name

	child = Toplevel(top)
	child.geometry('200x200')

	frame1 = Frame(child)
	frame1.pack()

	canvas = Canvas(frame1, bg="blue", height=200, width=200)
	canvas.pack(side="left")

	post_img = Image.open(path)
	post_img = post_img.resize((200,200), Image.ANTIALIAS)
	post_img = ImageTk.PhotoImage(post_img)
	image = canvas.create_image(0,0,anchor=NW, image = post_img)'''



def preUpload():
	global pre
	global post 
	global pre_entry
	global pre_canvas
	global pre_path
	global pre_img

	# pre_path = "dataset/pre/"

	# pre_path = pre_path + pre_entry.get()
	pre_path = askopenfilename(filetypes=(("Template files", "*.jpg"),
										   ("HTML files", "*.html;*.htm"),
										   ("All files", "*.*") ))
	try:
		pre_img = Image.open(pre_path)
		pre_img = pre_img.resize((200,200), Image.ANTIALIAS)
		pre_img = ImageTk.PhotoImage(pre_img)
		image = pre_canvas.create_image(0,0,anchor=NW, image = pre_img)

		pre = cv2.imread(pre_path,1)
	except AttributeError :
		print("")

def postUpload():
	global post 
	global post_entry
	global post_canvas
	global post_path
	global post_img

	# post_path = "dataset/post/"

	# post_path = post_path + post_entry.get()
	post_path = askopenfilename(filetypes=(("Template files", "*.jpg"),
										   ("HTML files", "*.html;*.htm"),
										   ("All files", "*.*") ))
	try:
		post_img = Image.open(post_path)
		post_img = post_img.resize((200,200), Image.ANTIALIAS)
		post_img = ImageTk.PhotoImage(post_img)
		image = post_canvas.create_image(0,0,anchor=NW, image = post_img)
	except AttributeError:
		print("")

	post = cv2.imread(post_path,1)
	head, tail = os.path.split(post_path)
	if tail[17]=='R':
	   post = cv2.flip(post,1)

def save_image():
	global pre_path
	global post_path
	global pre_encodings
	global post_encodings

	head, tail = os.path.split(pre_path)
	pre_name = tail

	head, tail = os.path.split(post_path)
	post_name = tail

	save_encodings(pre_name, pre_encodings, post_name, post_encodings)

################################################################################


# global variables
pre_encodings = numpy.zeros(128, dtype=numpy.float64)
post_encoding = numpy.zeros(128, dtype=numpy.float64)

#pre = cv2.imread("pre.jpg")
#post = cv2.imread("post.jpg")

template = cv2.imread("templet2.jpg", 1)
rect = dlib.rectangle(0,0,template.shape[0],template.shape[0])

path=""
canvas=""

pre_img = ""
post_img = ""




post_path = ""
pre_path = ""
'''files = glob.glob(post_path)
compared = open("encodings/pre_post_compared.txt","a")
i = 1'''






top = Tk()
top.geometry('400x400')

frame = Frame(top)
frame.pack()

pre_canvas = Canvas(frame, bg="blue", height=200, width=200)
pre_canvas.pack(side="left")

post_canvas = Canvas(frame, bg="blue", height=200, width=200)
post_canvas.pack(side="right")


pre_upload = Button(top, text="Upload", command=preUpload)
pre_upload.place(x=0,y=220)

post_upload = Button(top, text="Upload", command=postUpload)
post_upload.place(x=328,y=220)




pre_forgery_button = Button(top, text=" Pre-Forgery Detection" , command=pre_forgery_detection)
pre_forgery_button.place(x=0,y=260)

post_forgery_button = Button(top, text="Post-Forgery Detection" , command=post_forgery_detection)
post_forgery_button.place(x=238,y=260)


compare_face_button = Button(top, text="Compare Pre and Post", command=compare)
compare_face_button.place(x=115,y=300)

compare_with_pre_dataset_button = Button(top, text="Compare with pre dataset", command=compare_with_pre_dataset)
compare_with_pre_dataset_button.place(x=5,y=340)

compare_with_pre_dataset_button = Button(top, text="Compare with post dataset", command=compare_with_post_dataset)
compare_with_pre_dataset_button.place(x=205,y=340)

save = Button(top, text="Save",command=save_image)
save.place(x=170,y=370)

top.mainloop()






'''for file in files:
	head, tail = os.path.split(file)
	post = tail
	pre_img = pre_path+'PRE'+post[4:]

	post_name = tail
	head, tail = os.path.split(pre_img) 2.04046202e-12
	pre_name = tail

	try:
		pre = cv2.imread(pre_img, 1)
		post = cv2.imread(file, 1)

		#im1, im2 = annotate_landmarks(pre, post)
		#showImage(im1, im2)


		pre, post = resize(pre, post, template, rect)
		#im1, im2 = annotate_landmarks(pre, post, rect)
		#showImage(im1, im2)

		pre = histoEqu(pre)
		post = histoEqu(post)

		#pre = cv2.bilateralFilter(pre,9,75,75)
		#post = cv2.bilateralFilter(post,9,75,75)

		#pre = adjust_gamma(pre)
		#post = adjust_gamma(post)

		pre_encodings = get_pre_encodings(pre, rect)
		post_encodings = get_post_encodings(post, rect)
		distance = face_recognition.face_distance([post_encodings], pre_encodings)
		print(tail,distance,i)
		compared.write(tail + str(distance) + str(i))
		compared.write("\n")

		save_image(pre_name, pre_encodings, post_name, post_encodings)
		#check_if_already_present(pre_encodings)
		#pre, post = annotate_landmarks(pre, post, rect)
		#showImage(pre, post)
		i+=1
	except NoFaces:
		print("noface")
	except TooManyFaces:
		print("tomanyfaces")
	except Exception as e:
		print(e)
compared.close()'''